import React from 'react';
import './App.css';
import Button from "./components/Button";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      number1: 0,
      result: "0"
    }
  }

  updateResult(e) {
    // Check if enter 0 first number
    if (Number.isInteger(parseInt(e.target.innerText)) && this.state.result == "0") {
      this.setState({
        result: e.target.innerText
      })
    } else {
      this.setState((state) => ({
        result: state.result + e.target.innerText
      }))
    }
  }

  AC() {
    this.setState({
      result: "0"
    })
  }

  calculator() {
    const numbers = this.state.result.split(/[-/*+]+/).filter(obj => !obj == "").map(obj => parseInt(obj))
    const operators = this.state.result.split(/[0-9]+/).filter(obj => !obj == "")

    if (operators.length > 0) {
      let calc = 0;
      if (this.state.number1 == 0) {
        if (operators[0] == "/") {
          calc = numbers[0] / numbers[1]
        } else if (operators[0] == "*") {
          calc = numbers[0] * numbers[1]
        } else if (operators[0] == "-") {
          calc = numbers[0] - numbers[1]
        } else if (operators[0] == "+") {
          calc = numbers[0] + numbers[1]
        }
        this.setState({number1: calc, result: calc})
      } else {
        if (operators[0] == "/") {
          calc = this.state.number1 / numbers[1]
        } else if (operators[0] == "*") {
          calc = this.state.number1 * numbers[1]
        } else if (operators[0] == "-") {
          calc = this.state.number1 - numbers[1]
        } else if (operators[0] == "+") {
          calc = this.state.number1 + numbers[1]
        }
        this.setState({number1: calc, result: calc})
      }
    }

  }

  render() {
    return (
      <div>
        <div className='calculator column'>
          <span>{this.state.result}</span>
          <div className='row'>
            <div className='column'>
              <div className='row'>
                <Button value="AC" onPress={() => this.AC()} style={{ "backgroundColor": "gray" }} />
                <Button value="+/-" onPress={(e) => this.updateResult(e)} style={{ "backgroundColor": "gray" }} />
                <Button value="%" onPress={(e) => this.updateResult(e)} style={{ "backgroundColor": "gray" }} />
              </div>
              <div className='row' style={{ "flexWrap": "wrap" }}>
                <Button value="7" onPress={(e) => this.updateResult(e)} />
                <Button value="8" onPress={(e) => this.updateResult(e)} />
                <Button value="9" onPress={(e) => this.updateResult(e)} />
                <Button value="4" onPress={(e) => this.updateResult(e)} />
                <Button value="5" onPress={(e) => this.updateResult(e)} />
                <Button value="6" onPress={(e) => this.updateResult(e)} />
                <Button value="1" onPress={(e) => this.updateResult(e)} />
                <Button value="2" onPress={(e) => this.updateResult(e)} />
                <Button value="3" onPress={(e) => this.updateResult(e)} />
                <Button value="0" onPress={(e) => this.updateResult(e)} style={{ "width": "200px" }} />
                <Button value="." onPress={(e) => this.updateResult(e)} />
              </div>
            </div>
            <div className='column'>
              <Button value="/" onPress={(e) => { this.updateResult(e); this.calculator(e) }} style={{ "backgroundColor": "orange", "color": "white" }} />
              <Button value="*" onPress={(e) => { this.updateResult(e); this.calculator(e) }} style={{ "backgroundColor": "orange", "color": "white" }} />
              <Button value="-" onPress={(e) => { this.updateResult(e); this.calculator(e) }} style={{ "backgroundColor": "orange", "color": "white" }} />
              <Button value="+" onPress={(e) => { this.updateResult(e); this.calculator(e) }} style={{ "backgroundColor": "orange", "color": "white" }} />
              <Button value="=" onPress={() => this.calculator()} style={{ "backgroundColor": "orange", "color": "white" }} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App;
